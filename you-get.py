#!/usr/bin/env python3
# coding=utf-8
import os
import re
import sys
import appex
import console
import clipboard
from you_get import common as ygt
import urllib.request, urllib.error, urllib.parse
from urllib.parse import quote

dir_s = '~/Documents/Videos/'
path_s = os.path.expanduser(dir_s)
# path_tmp = os.path.join(path_save,'tmp')
if not os.path.exists(path_s):
    os.makedirs(path_s)

if appex.is_running_extension() and re.search(
        'https*:\/\/[^\s]+', appex.get_attachments()[0]) is not None:
    url = appex.get_attachments()[0]
else:
    clip = re.search('https*:\/\/[^\s]+', clipboard.get())
    if clip is None:
        url = console.input_alert('URL Input')
    else:
        url = clipboard.get()
# print ('开始转换URL...')
try:
    r = urllib.request.urlopen('%s' % quote(url, safe='/:?='))
    url_true = r.geturl()
except:
    url_true = url
print('查找可用格式...')
try:
    sys.argv = ['ygt', '-i', url_true]
    ygt.main()
    option = input(
        '手动输入示例\n--itag=137\n--format=flv\n--debug\n自动下载最佳格式直接回车！\nOption Input: '
    ).strip()
    if option.startswith('-'):
        print('开始下载指定格式')
        sys.argv = ['ygt', option, '-o', path_s, url_true]
    else:
        print('开始下载最佳格式')
        sys.argv = ['ygt', '-o', path_s, url_true]
    ygt.main()
except Exception as e:
    print(e)
